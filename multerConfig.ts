import multer from "multer"
import { resolve } from 'path'

export const storage = multer.diskStorage({
    destination: resolve('.', 'uploads', 'temp'),
    filename: (_request, file, callback) => {
        const time = Date.now()

        return callback(null, `${time}_${file.originalname}`)
    }
})
