import express from 'express'
import multer from 'multer'
import { storage } from './multerConfig'
import { join, resolve } from 'path'
import { MergeWithIntro, ResizeVideo } from './src/video'
import { onError } from './src/utils'

const upload = multer({ storage })
const app = express()

app.get('/', (_request, response) => {
    return response.sendFile(join(__dirname, 'public', 'index.html'))
})

app.post(
    '/upload',
    upload.single('file'),
    async (request, response) => {
        try {
            if (typeof request.file === 'undefined') {
                throw new Error(`File uploaded does not exists`)
            }

            await MergeWithIntro(request.file.filename, true)

            return response.json({
                filename: request.file.filename,
            })
        } catch (error: any) {
            onError(error)

            if (error instanceof Error) {
                return response.json({
                    error: error.message
                })
            }
            return response.json({ error })
        }
    }
)

app.listen(3000)
