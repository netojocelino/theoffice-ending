import { join, resolve } from 'path'
import fs from 'fs/promises'
import ffmpeg from "fluent-ffmpeg"

import { onError } from './utils'

const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path
const ffProbePath = require('@ffprobe-installer/ffprobe').path

ffmpeg.setFfmpegPath(ffmpegPath)
ffmpeg.setFfprobePath(ffProbePath)

const VIDEO_SIZE = '480x270'

const FOLDERS = {
    OUTPUT: './uploads/output',
    TEMP: './uploads/temp',
}


function getTemporaryName(filename: string, prefix: string = '') {
    return join(resolve('.', 'uploads', 'temp'), `${prefix}${filename}`)
}


export function ResizeVideo(filename: string, callback?: any) {
    const videoLocation = getTemporaryName(filename)

    const input = ffmpeg({  source: videoLocation })
    const resized = input.setSize(VIDEO_SIZE)

    const resizedName = getTemporaryName(filename, 'resized-')
    resized.saveToFile(resizedName)

    if (callback !== undefined && typeof callback === 'function') {
        callback(resized)
    }
    return resized
}

export async function MergeWithIntro (filename: string, mustDelete: boolean = false) {
    let intro: string | undefined
    const fileLocation = getTemporaryName(filename)

    try {        
        await fs.stat(fileLocation).catch(_err => {
            throw new Error(`File ${filename} does not exists`)
        })

        intro = join(resolve('.', 'uploads'), 'intro.mp4')
        await fs.stat(intro).catch(_err => {
            throw new Error(`File ${intro} does not exists`)
        })

    } catch(error: any) {
        onError(error)
        throw error
    }

    try {
        const outputName = 'the-office-intro-combined_'+Date.now() + '.mp4'

        ffmpeg(fileLocation)
            .size(VIDEO_SIZE)
            .videoCodec('copy')
            .input(intro)
            .size(VIDEO_SIZE)
            .videoCodec('copy')

            .on('error', (err) => { throw err })
            .on('start', () => {
                console.log(`Starting merge for ${outputName}`)
            })
            .on('end', () => {
                console.log(`${outputName} merged!`)
                return resolve()
            })
            .mergeToFile(join(__dirname, '..', FOLDERS.OUTPUT, outputName), <any> FOLDERS.TEMP)

    } catch (error: any) {
        throw error
    }

    if (mustDelete) {
        fs.unlink(fileLocation)
    }

}
