export function isNullOrUndefined( obj: any ) {
    return obj === null || typeof obj === 'undefined'
}

export function isObject( obj: any ) {
    return !isNullOrUndefined(obj) && typeof obj === 'object'
}

export function onError (err: Error) {
    if (isObject(err)) {
        console.error(`Error ${err.message}`, '\n')
    } else {
        console.error(err, '\n')
    }

    process.exitCode = 1
}


export default {
    isNullOrUndefined,
    isObject,
    onError,
}
